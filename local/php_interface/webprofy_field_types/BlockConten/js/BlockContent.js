/*
 var BlockContentInitialized = false;

 function setBlockContent(uid){
 var content = {};
 var is_block = false;
 $(".block-content[data-uid='"+uid+"'] .block-content-block.active").each(
 function(){
 content[$(this).attr("data-uid")] = {
 "uid":$(this).attr("data-uid"),
 "content":$(this).attr("data-content"),
 "type":$(this).attr("data-type")
 };
 is_block = true;
 }
 );
 if(is_block){
 $("#field_"+uid).val($.base64.encode(JSON.stringify(content)));
 }else{
 $("#field_"+uid).val("");
 }
 }
 function getBlockContent(){
 $(".block_content_field.initialized:not(.geted)").each(
 function(){
 if($(this).val()){
 var content = JSON.parse($.base64.decode($(this).val()));
 var uid = $(this).attr("data-uid");
 for(i in content){
 switch(content[i].type){
 case "text":
 var data = decodeURIComponent($.base64.decode(content[i].content));
 break;
 }
 $(".block-content[data-uid='"+uid+"'] .block-content-blocks").append(
 [
 '<div class="block-content-block active '+content[i].type+'" data-type="'+content[i].type+'" data-content="'+content[i].content+'" data-uid="'+content[i].uid+'">',
 '<div class="block-content-handle"></div>',
 '<div class="block-content-remove"></div>',
 '<div class="block-content-data">'+data+'</div>',
 '</div>'
 ].join("")
 );
 }
 $(this).addClass("geted");
 }
 }
 );
 initBlockContentSortable();
 }

 function initBlockContentSortable(){
 $(".block-content").each(
 function(){
 $(this).find(".block-content-blocks").sortable({
 handle: ".block-content-handle",
 change: function(){
 var uid = $(this).closest(".block-content").attr("data-uid");
 setTimeout(
 function(){
 setBlockContent(uid);
 },
 1000
 );
 }
 });
 $(this).find(".block-content-blocks").disableSelection();
 }
 );
 }

 function initBlockContent(){
 $(".block_content_field:not(.initialized)").each(
 function(){
 var uid = $(this).attr("data-uid");
 $(this).before(
 [
 '<div class="block-content" data-uid="'+uid+'">',
 '<div class="block-content-header">',
 '<div class="block-content-remove"></div>',
 '</div>',
 '<div class="block-content-body">',
 '<div class="block-content-blocks"></div>',
 '<div class="block-content-plus text" data-type="text" data-content="">Текст</div>',
 '<div class="block-content-plus image" data-type="image" data-content="">Изображение</div>',
 '<div class="block-content-plus video" data-type="video" data-content="">Видео</div>',
 '</div>',
 '<div class="block-content-footer">',
 '</div>',
 '</div>'
 ].join("")
 );
 $(this).addClass("initialized");
 }
 );
 getBlockContent();
 if(!BlockContentInitialized){
 BlockContentInitialized = true;
 $(".block-content:first").closest(".adm-detail-content-cell-r").parent().before('<tr class="heading"><td colspan="2">'+$(".block-content:first").closest(".adm-detail-content-cell-r").prev().text()+'</td></tr>');
 $(".block-content:first").closest(".adm-detail-content-cell-r").prev().remove();
 $(".block-content:first").closest(".adm-detail-content-cell-r").attr("colspan","2");
 $(document).on("click",".block-content-header .block-content-remove",
 function(){
 if($(".block_content_field").size()>1){
 $(this).closest("tr").remove();
 }
 return false;
 }
 );
 $(document).on("click",".block-content-block .block-content-remove",
 function(){
 var uid = $(this).closest(".block-content").attr("data-uid");
 $(this).closest(".block-content-block").remove();
 setBlockContent(uid);
 initBlockContentSortable();
 return false;
 }
 );
 $(document).on("click",".block-content .block-content-body .block-content-plus",
 function(){
 var type = $(this).attr("data-type");
 var d = new Date();
 var id = d.getTime()+"_"+Math.round(Math.random()*1000000);
 $(this).closest(".block-content-body").find(".block-content-blocks").append(
 [
 '<div class="block-content-block active '+type+'" data-type="'+type+'" data-content="" data-uid="'+id+'">',
 '<div class="block-content-handle"></div>',
 '<div class="block-content-remove"></div>',
 '<div class="block-content-data"></div>',
 '</div>'
 ].join("")
 );
 initBlockContentSortable();
 return false;
 }
 );
 $(document).on("click",".block-content .block-content-body .block-content-block.active",
 function(){
 var that = $(this);
 var type = that.attr("data-type");
 var content = that.attr("data-content");
 var uid = that.closest(".block-content").attr("data-uid");
 switch(type){
 case "text":
 var d = new Date();
 var id = d.getTime()+"_"+Math.round(Math.random()*1000000);
 var content = decodeURIComponent($.base64.decode(content));
 var dialog = new BX.CAdminDialog({
 'content_url': '/ajax/BlockConten/TextEditor.php',
 'content_post': {
 'ID':id,
 'CONTENT':content,
 'INPUT_NAME':'field_'+id,
 'INPUT_ID':'field_'+id
 },
 'draggable': true,
 'resizable': true,
 'height': screen.height / 2,
 'width': screen.width - 200,
 'buttons': [
 {
 title: 'Сохранить',
 id: 'savebtn',
 name: 'savebtn',
 className: 'adm-btn-save',
 action: function () {
 that.attr("data-content",$.base64.encode(encodeURIComponent($("input#bxed_"+id).val())));
 that.find(".block-content-data").html($("input[name='field_"+id+"']").val());
 setBlockContent(uid);
 dialog.Close();
 }
 }, BX.CAdminDialog.btnCancel]
 });
 dialog.Show();
 break;
 case "image":
 var d = new Date();
 var id = d.getTime()+"_"+Math.round(Math.random()*1000000);
 var dialog = new BX.CAdminDialog({
 'content_url': '/ajax/BlockConten/ImageEditor.php',
 'content_post': {
 'ID':id,
 'CONTENT':'',
 'INPUT_NAME':'field_'+id,
 'INPUT_ID':'field_'+id
 },
 'draggable': true,
 'resizable': true,
 'height': screen.height / 2,
 'width': screen.width - 200,
 'buttons': [
 {
 title: 'Сохранить',
 id: 'savebtn',
 name: 'savebtn',
 className: 'adm-btn-save',
 action: function () {

 }
 }, BX.CAdminDialog.btnCancel]
 });
 dialog.Show();
 break;
 }
 return false;
 }
 );
 }
 }
 BX.ready(function(){
 initBlockContent();
 });
 */
var BlockContent = function(UID){
	this.UID = UID;

	this.initTextField = function(UID, data){
		var e = $(".block-content-block[data-uid='"+data.uid+"'] .block-content-data");
		var name = $(".block_content_field[data-uid='"+UID+"']").data("name");
		e.load("/local/php_interface/webprofy_field_types/BlockConten/ajax/TextEditor.php?",{
            'ID':data.uid,
            'CONTENT': data.content,
            'INPUT_NAME': name+'['+data.uid+']',
            'INPUT_ID':'block_field_'+data.uid,
            'JS_OBJ_NAME': 'js_'+data.uid
        });
	}

	this.initImageField = function(UID, data){
		var e = $(".block-content-block[data-uid='"+data.uid+"'] .block-content-data");
		var name = $(".block_content_field[data-uid='"+UID+"']").data("name");

		e.load("/local/php_interface/webprofy_field_types/BlockConten/ajax/ImageEditor.php?",{
				'ID':data.uid,
				'COUNT':1,
				'IMAGE': data.content,
				'INPUT_NAME': name+'['+data.uid+']',
				'TYPE': data.type,
				'VIEW': data.view,
				'URL': data.url
			});
	}

	this.initVideoField = function(UID, data){
		var e = $(".block-content-block[data-uid='"+data.uid+"'] .block-content-data");
		var name = $(".block_content_field[data-uid='"+UID+"']").data("name");

		e.load("/local/php_interface/webprofy_field_types/BlockConten/ajax/VideoEditor.php?",{
				'ID':data.uid,
				'COUNT':1,
				'IMAGE': data.content.image,
				'VIDEO': data.content.video,
				'INPUT_NAME': name+'['+data.uid+']',
				'TYPE': data.type
			});
	}

	this.initGalleryField = function(UID, data){
		var e = $(".block-content-block[data-uid='"+data.uid+"'] .block-content-data");
		var name = $(".block_content_field[data-uid='"+UID+"']").data("name");
		e.load("/local/php_interface/webprofy_field_types/BlockConten/ajax/ImageEditor.php?",{
				'ID':data.uid,
				'COUNT':10,
				'IMAGE': data.content,
				'INPUT_NAME': name+'['+data.uid+']',
				'TYPE': data.type,
				'GALLERY': data.gallery
			});
	}

	this.addField = function(UID, data){
		$(".block-content[data-uid='"+UID+"'] .block-content-blocks").append(
			[
				'<div class="block-content-block active '+data.type+'" data-type="'+data.type+'" data-content="'+data.content+'" data-uid="'+data.uid+'">',
				'<div class="block-content-handle"></div>',
				'<div class="block-content-remove"></div>',
				'<div class="block-content-data">'+data.content+'</div>',
				'</div>'
			].join("")
		);

		switch(data.type){
			case "text":
				this.initTextField(UID, data);
				break;
			case "image":
				this.initImageField(UID, data);
				break;
			case "gallery":
				this.initGalleryField(UID, data);
				break;
			case "video":
				this.initVideoField(UID, data);
				break;
		}
	}

	this.removeField = function(UID){
		var field = $(".block-content-block[data-uid='"+UID+"']");
		if(field.data("type") == "image" || field.data("type") == "gallery" || field.data("type") == "video")
		{
			field.find('.adm-btn-del').trigger('click');
		}
		$(".block-content-block[data-uid='"+UID+"']").remove();
	}

	this.reInitField = function(item) {
		var UID = item.closest('.block-content').data('uid');
		var type = item.data('type');
		var new_content = item.find('input[type="hidden"]').val();

		item.find('.block-content-data').empty();

		var data = {
			uid: item.data('uid'),
			type: type,
			content: new_content
		};

		switch(type){
			case "text":
				this.initTextField(UID, data);
				break;
			case "image":
				this.initImageField(UID, data);
				break;
			case "gallery":
				this.initGalleryField(UID, data);
				break;
			case "video":
				this.initVideoField(UID, data);
				break;
		}
	}

	this.Init = function(){
		var e = $("#field_"+this.UID);

		e.before(
			[
				'<div class="block-content" data-uid="'+this.UID+'">',
				'<div class="block-content-body">',
				'<div class="block-content-blocks"></div>',
				'<div class="block-content-plus text" data-type="text" data-content="">Текст</div>',
				'<div class="block-content-plus image" data-type="image" data-content="">Изображение</div>',
				'<div class="block-content-plus gallery" data-type="gallery" data-content="">Галерея</div>',
				'<div class="block-content-plus video" data-type="video" data-content="">Видео</div>',
				'</div>',
				'<div class="block-content-footer">',
				'</div>',
				'</div>'
			].join("")
		);

		$(".block-content[data-uid='"+this.UID+"'] .block-content-plus").on('click',
			function(){
				var type = $(this).attr("data-type");
				var d = new Date();
				var id = d.getTime()+"_"+Math.round(Math.random()*1000000);
				var uid = $(this).closest(".block-content").attr("data-uid");
				BC.addField(uid,{type:type,uid:id,content:""});
			}
		);

		$( ".block-content-blocks" ).sortable({
			placeholder: "portlet-placeholder ui-corner-all",
			stop: function( event, ui){
				if(ui.item.data('type') == 'text')
					BC.reInitField(ui.item);
			}
		});

		$( ".block-content-blocks, .block-content-block, .block-content" ).disableSelection();

		$(document).on('click', '.block-content-remove',
			function(){
				var uid = $(this).closest(".block-content-block").attr("data-uid");
				BC.removeField(uid);
			}
		);

		$(document).on('click', '.js-load-video',
			function(){
				var block = $(this).closest(".block-content-block");
				var src = block.find('.js-input-video').val();
				var match = src.match(/\?v=(.*)$/);

				if(match !== null && match[1] != '')
					src = 'https://www.youtube.com/embed/' + match[1];
				else if(src != '')
					src = 'https://www.youtube.com/embed/' + src;

				if(src != '')
					block.find('.js-content-video').html('<iframe width="560" height="315" src="'+src+'" frameborder="0" allowfullscreen></iframe>');
				else
					block.find('.js-content-video').html('');
			}
		);


		/*		$(document).on('click', '.adm-btn-del',
		 function(){
		 alert('ddd');
		 var field = $(".block-content-block");
		 var uid = field.data('uid');
		 if(field.data("type") == "video")
		 {
		 var name = field.find('.block_content_field').data('name');
		 $('[name="'+name+'[content]"]').attr('name', name+'[content_del]');
		 }
		 }
		 );*/

		try{
			var content = JSON.parse($.base64.decode(e.val()));
			var d = new Date();
			for(i in content){
				var n = d.getTime();
				content[i].uid = $.base64.encode(n)+"_"+Math.round(Math.random()*1000000)+'_'+i;
				this.addField(this.UID, content[i]);
			}
		}catch(error){}

		e.addClass("initialized");
		e = e.closest(".adm-detail-content-cell-r");
		e.parent().before('<tr class="heading"><td colspan="2">'+e.prev().text()+'</td></tr>');
		e.prev().remove();
		e.attr("colspan","2");
	}
	this.Init();
	delete this.Init;

	return this;
}

var BC;

BX.ready(function(){
	$(".block_content_field:not(.initialized)").each(
		function(){
			BC = new BlockContent($(this).attr("data-uid"));
		}
	);
});
