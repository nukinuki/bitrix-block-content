<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php"); 
CModule::IncludeModule("form");

$show_image = htmlspecialcharsbx($_REQUEST['IMAGE']);
$after_name = '[content]';
if($_REQUEST['TYPE'] == 'gallery') {
	?>
	<select name="<?=htmlspecialcharsbx($_REQUEST['INPUT_NAME']).'[gallery]';?>" class="block-content-select">
		<option value="vertical"<? if($_REQUEST['GALLERY'] == 'horizontal') echo ' selected';?>>Вертикальная</option>
		<option value="horizontal"<? if($_REQUEST['GALLERY'] == 'horizontal') echo ' selected';?>>Горизонтальная</option>
	</select>
	<?
	$after_name = '[n#IND#]';

	$show_image = array();
	foreach($_REQUEST['IMAGE'] as $key=>$image)
	{
		$show_image[$_REQUEST['INPUT_NAME'].'[content]['.$key.']'] = $image;
	}

}
else {
	?>
	Стиль вывода:
	<select name="<?=htmlspecialcharsbx($_REQUEST['INPUT_NAME']).'[view]';?>" class="block-content-select">
		<option value="">Не задан</option>
		<option value="nosection"<? if($_REQUEST['VIEW'] == 'nosection') echo ' selected';?>>Без обёртки</option>
		<option value="browser"<? if($_REQUEST['VIEW'] == 'browser') echo ' selected';?>>В окне браузера</option>
	</select>
	<br>
	Ссылка: <input type="text" name="<?=htmlspecialcharsbx($_REQUEST['INPUT_NAME']).'[url]';?>" value="<?=htmlspecialcharsbx($_REQUEST['URL'])?>">
	<br>
	<br>
	<?
}

echo \Bitrix\Main\UI\FileInput::createInstance(array(
				"name" => htmlspecialcharsbx($_REQUEST['INPUT_NAME']).$after_name, //имя должно быть уникально
				"description" => true, //разрешить устанавливать description
				"upload" => true, //запрещает загрузку
				"medialib" => true, //разрешить выбрать из медиабиблиотеки
				"fileDialog" => true,
				"cloud" => true,
				"delete" => true, //можно удалять элемент
				"maxCount" => htmlspecialcharsbx($_REQUEST['COUNT']), //кол-во эл-в
				"allowUpload" => "A", //может принимать значения A,F,I (A,F - файлы, I - картинка)
				//"allowUploadExt" => ".png", //устанавливает допустимое расширение загружаемого файла
				"allowSort" => "Y" //можно сортировать эл-ты
			))->show($show_image);

?>
<input type="hidden" name="<?=htmlspecialcharsbx($_REQUEST['INPUT_NAME'])?>[type]" value="<?=htmlspecialcharsbx($_REQUEST['TYPE'])?>">
