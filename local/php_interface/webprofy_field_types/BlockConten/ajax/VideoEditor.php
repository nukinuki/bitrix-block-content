<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("form");

$show_image = htmlspecialcharsbx($_REQUEST['IMAGE']);
$video = htmlspecialcharsbx($_REQUEST['VIDEO']);
$video_embed = '';
preg_match("/\?v=(.*)$/", $video, $match);

if(isset($match) && $match[1] != '')
{
    $video_embed = 'https://www.youtube.com/embed/' . $match[1];
}
else if($video != '')
{
    $video_embed = 'https://www.youtube.com/embed/' . $video;
}


$after_name = '[content][image]';
?>
<table class="block-content-table">
    <tr>
        <td>
            <label for="<?=htmlspecialcharsbx($_REQUEST['INPUT_NAME'])?>_video">Ссылка на видео с youtube</label><br>
            <input id="<?=htmlspecialcharsbx($_REQUEST['INPUT_NAME'])?>_video" type="text" name="<?=htmlspecialcharsbx($_REQUEST['INPUT_NAME'])?>[video]" class="js-input-video" value="<?=$video?>">
            <input type="button" name="video_load" value="Предпросмотр видео" class="js-load-video">
        </td>
        <td>
            <div class="block-content-video-holder js-content-video">
                <?
                if($video_embed != ''){
                    ?>
                    <iframe width="560" height="315" src="<?=$video_embed?>" frameborder="0" allowfullscreen></iframe>
                <?}?>
            </div>
        </td>
    </tr>
</table>
<label>Превью для видео</label><br>
<?
echo \Bitrix\Main\UI\FileInput::createInstance(array(
    "name" => htmlspecialcharsbx($_REQUEST['INPUT_NAME']).$after_name, //имя должно быть уникально
    "description" => true, //разрешить устанавливать description
    "upload" => true, //запрещает загрузку
    "medialib" => true, //разрешить выбрать из медиабиблиотеки
    "fileDialog" => true,
    "cloud" => true,
    "delete" => true, //можно удалять элемент
    "maxCount" => htmlspecialcharsbx($_REQUEST['COUNT']), //кол-во эл-в
    "allowUpload" => "A", //может принимать значения A,F,I (A,F - файлы, I - картинка)
    //"allowUploadExt" => ".png", //устанавливает допустимое расширение загружаемого файла
    "allowSort" => "Y" //можно сортировать эл-ты
))->show($show_image);

?>

<input type="hidden" name="<?=htmlspecialcharsbx($_REQUEST['INPUT_NAME'])?>[type]" value="<?=htmlspecialchars($_REQUEST['TYPE'])?>">
