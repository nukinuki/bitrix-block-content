<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php"); 
CModule::IncludeModule("fileman");
/*
do{
	$content = explode(" ",htmlspecialcharsbx($_REQUEST['CONTENT']));
	if(strlen($content[0])>50){
		$_REQUEST['CONTENT'] = base64_decode($_REQUEST['CONTENT']);
	}
}while(strlen($content[0])>50);
$content = htmlspecialcharsbx($_REQUEST['CONTENT']);*/

if (!function_exists('validBase64')) {
	function validBase64($string) {
		if (empty($string)) return true;

		$decoded = base64_decode($string, true);

		// Check if there is no invalid character in string
		if (!preg_match('/^[a-zA-Z0-9\/\r\n+]*={0,2}$/', $string)) return false;

		// Decode the string in strict mode and send the response
		if (!base64_decode($string, true)) return false;

		// Encode and compare it to original one
		if (base64_encode($decoded) != $string) return false;

		return true;
	}
}

//if (validBase64($_REQUEST['CONTENT'])) {
	//$content = base64_decode($_REQUEST['CONTENT']);
//} else {
//	$content = $_REQUEST['CONTENT'];
//}
$content = $_REQUEST['CONTENT'];

$LHE = new CHTMLEditor;
$LHE->Show(array(
	'id' => $_REQUEST['ID'],
	'content' => $content,
	'inputName' => $_REQUEST['INPUT_NAME'].'[content]',
	'inputId' => $_REQUEST['INPUT_ID'],
	'width' => '100%',
	'height' => '500',
	'bUseFileDialogs' => false,
	'jsObjName' => $_REQUEST['JS_OBJ_NAME'],
	'toolbarConfig' => array(
	),
	'videoSettings' => false,
	'bResizable' => false,
	'bAutoResize' => false
));
?>
<input type="hidden" name="<?=htmlspecialcharsbx($_REQUEST['INPUT_NAME'])?>[type]" value="text">
<script>
$(function(){
	$(".bxhtmled-taskbar-cnt[data-bx-type='taskbarmanager']").remove();
});
</script>
