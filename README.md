# Блочный редактор контента

Модуль добавляет новый тип свойства "Блочный контент" и редактор для этого свойства.

Общий принцип следующий: вместо того, чтобы работать с одним большим полем в визульном редакторе
администратор может собирать контент из последовательных блоков. Каждый из этих блоков может иметь
свою HTML-разметку и набор свойств. Например для вставки видео-плеера из Youtube достаточно добавить
блок "Видео" и указать ссылку на видео-ролик.

# Установка

Распакуйте в корень. Подключите в init.php:

```
require_once($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/webprofy_field_types/BlockConten/BlockContent.php");
```

# Использование

Добавьте в нужный инфоблок свойство типа "Блочный контент".

## TODO

- Переделать в модуль
- Упростить визуальный редактор
- Добавить настройки
- Вынести каждый отдельный тип блока (Видео, Текст и т. д.) в отдельный подключаемый файл-плагин
- Добавить в визуальный редактор кнопку "Разделить блок", при нажатии на которую блок будет
делится на два в том месте, где был установлен курсор
- Добавить в визуальный редактор кнопку "Объединить с нижним блоком", при нажатии на которую
блок будет объединяться с нижним блокам, если оба блока текстовые

